#include <iostream>

#include "FFmpegAPI.h"

int main (int argc, char *argv[])
{
	using namespace TT::ProtoLib;

	//very elementary argument handling, it is enough for now 
	if ((argc < 2) || (argv[argc - 1] == nullptr)) {
		std::cerr << "Please, give media filename with full path" << std::endl;
		return 1;
	}

	std::string filename = argv[argc-1];

	FFmpegAPI lib(filename);

	std::cout << "Filename : " << lib.getFilename() << std::endl;
	std::cout << "Format   : " << lib.getFormatString() << std::endl;
	std::cout << "Duration : " << lib.getDurationSeconds() << " sec" << std::endl;
	std::cout << "Streams  : " << lib.getStreamCount() << std::endl;

	auto streams = lib.getStreams();
	for (auto i : streams) {
		std::cout << "Type: " << i.type << " Duration: " << i.duration << " sec Fourcc: " + i.fourcc << " Codec: " << i.codec << std::endl;
	}

	return 0;
}
