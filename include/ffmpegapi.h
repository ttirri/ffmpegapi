//
// Copyright (c) 2020 tomi.tirri@gmail.com
//
// Public header file for FFMpeg-library API
//
// License: GNU Lesser General Public License version 2.1 or later (LGPL v2.1+).
// SPDX-License-Identifier: LGPL-2.1+
//
#ifndef _TT_FFMPEGAPI_H
#define _TT_FFMPEGAPI_H

#include <iostream>
#include <memory>
#include <vector>

namespace TT {
namespace ProtoLib {
inline namespace v1 {

class FFmpegAPIImpl;

class FFmpegAPI {

	public:
		explicit FFmpegAPI(std::string const &fileName);
		const std::string getFilename() const;

		// returns format string ot the media file
		const std::string getFormatString() const;

		// returns duration of media in seconds
		int64_t getDurationSeconds() const;

		// return number of stream in media
		int getStreamCount() const;

		// read-only struct describing the stream in media file, defined later in this file
		struct Stream;

		// returns vector which contains streams in media, see struct Stream earlier in this header-file
		const std::vector<FFmpegAPI::Stream> getStreams() const;
		~FFmpegAPI();

		// movable:
		FFmpegAPI(FFmpegAPI && rhs) noexcept;
		FFmpegAPI& operator=(FFmpegAPI && rhs) noexcept;

		// non-copyable
		FFmpegAPI(const FFmpegAPI& rhs) = delete;
		FFmpegAPI& operator=(const FFmpegAPI& rhs) = delete;

		struct Stream {
			const std::string type;         //audio or video
			const int64_t duration;         //duration in seconds
			const std::string fourcc;       //fourcc
			const std::string codec;        //name of the codec

			Stream(std::string type, int64_t duration, std::string fourcc, std::string codec) : type(std::move(type)), duration(duration), fourcc(std::move(fourcc)), codec(std::move(codec)) {}
		};

	private:
		std::unique_ptr<FFmpegAPIImpl> apiImpl;
};

} // namespace v1
} // namespace ProtoLib
} // namespace TT
#endif
