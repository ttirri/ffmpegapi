# README #

Simple FFmpeg C++ API for demonstration purposes

### What is this repository for ###

This repository contains C++-code for sample API accessing FFmpeg-libraries.
Source code is compiled and run tested in Ubuntu 18.04.

### Dependecies ###

* CMake minimum version 3.10.2
* C++-compiler (g++ or clang++)
* libavformat-dev version 3.4.6
* Cpputest

### Compilation ###

* run ./tools/compile.sh 

### Running testapp ###

* run once ./tools/download.sh to get mp4-testfile
* run ./tools/run.sh

### Running unit tests ###

* run ./tools/runtests.sh

### License ###

* GNU Lesser General Public License version 2.1 or later (LGPL v2.1+)
* SPDX-License-Identifier: LGPL-2.1+
