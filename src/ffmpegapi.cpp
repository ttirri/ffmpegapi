//
// Copyright (c) 2020 tomi.tirri@gmail.com
//
// License: GNU Lesser General Public License version 2.1 or later (LGPL v2.1+).
// SPDX-License-Identifier: LGPL-2.1+
//
#include "ffmpegapi.h"
#include "ffmpegapiimpl.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include <sstream>

namespace TT {
namespace ProtoLib {
inline namespace v1 {

FFmpegAPI::FFmpegAPI(std::string const & fileName) : apiImpl(new FFmpegAPIImpl(fileName))
{
}

FFmpegAPI::~FFmpegAPI() = default;
FFmpegAPI::FFmpegAPI(FFmpegAPI &&) noexcept = default;
FFmpegAPI& FFmpegAPI::operator=(FFmpegAPI &&) noexcept = default;

const std::string FFmpegAPI::getFilename() const
{
	return apiImpl->getFilename();
}

const std::string FFmpegAPI::getFormatString() const
{
	return apiImpl->getFormatString();
}

int64_t FFmpegAPI::getDurationSeconds() const
{
	return apiImpl->getDurationSeconds();
}

int FFmpegAPI::getStreamCount() const
{
	return apiImpl->getStreamCount();
}

const std::vector<FFmpegAPI::Stream> FFmpegAPI::getStreams() const
{
	return apiImpl->getStreams();
}

} // namespace v1
} // namespace ProtoLib
} // namespace TT
