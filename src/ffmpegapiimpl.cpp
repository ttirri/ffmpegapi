//
// Copyright (c) 2020 tomi.tirri@gmail.com
//
// License: GNU Lesser General Public License version 2.1 or later (LGPL v2.1+).
// SPDX-License-Identifier: LGPL-2.1+
//
#include "ffmpegapi.h"
#include "ffmpegapiimpl.h"

extern "C" {
#include "libavformat/avformat.h"
}

#include <sstream>

namespace TT {
namespace ProtoLib {
inline namespace v1 {

FFmpegAPIImpl::FFmpegAPIImpl(std::string const &fileName) : _filename(fileName), _duration_seconds(AV_NOPTS_VALUE)
{
	std::stringstream ss;
	ss << "file:" << _filename;
	auto url = ss.str();

	//initialize library, these are void
	av_register_all();
	avcodec_register_all();

	AVFormatContext *format_context = nullptr;
 	auto ret = avformat_open_input(&format_context, url.c_str(), nullptr, nullptr);
	if (ret < 0) {
		std::string error_string;
		error_string.resize(256, '\0');
		av_strerror(ret, &error_string[0], 256);
		throw std::runtime_error(error_string);
		return;
	}

#ifdef DEBUG
	av_dump_format(format_context, 0, url.c_str(), 0);
#endif

	_format_string = format_context->iformat->long_name;

	if (format_context->duration != AV_NOPTS_VALUE) {
		_duration_seconds = format_context->duration + (format_context->duration <= INT64_MAX - 5000 ? 5000 : 0);
	}

	for (auto i = 0; i < static_cast<int>(format_context->nb_streams); i++) {
		AVStream *stream = format_context->streams[i];
		AVCodecContext *avcontext;

		if ((avcontext = avcodec_alloc_context3(nullptr)) == nullptr) {
			avformat_close_input(&format_context);
			throw std::runtime_error("avcodec_alloc_context3 fails");
			return;
		}

		if ((ret = avcodec_parameters_to_context(avcontext, stream->codecpar)) < 0) {
			avcodec_free_context(&avcontext);
			avformat_close_input(&format_context);
			throw std::runtime_error("avcodec_parameters_to_context");
			return;
		}

		std::string fourcc;
		if (static_cast<bool>(avcontext->codec_tag)) {
			fourcc.resize(AV_FOURCC_MAX_STRING_SIZE, '\0');
			av_fourcc_make_string(&fourcc[0], avcontext->codec_tag);
		}

		auto codec_type = av_get_media_type_string(avcontext->codec_type);
		auto codec_name = avcodec_get_name(avcontext->codec_id);
		auto duration = stream->duration;

		FFmpegAPI::Stream new_stream(codec_type, duration, fourcc, codec_name);
		_streams.push_back(new_stream);

		avcodec_free_context(&avcontext);
	}
	avformat_close_input(&format_context);
}

const std::string FFmpegAPIImpl::getFilename() const
{
	return _filename;
}

const std::string FFmpegAPIImpl::getFormatString() const
{
	return _format_string;
}

int64_t FFmpegAPIImpl::getDurationSeconds() const
{
	return _duration_seconds;
}

int FFmpegAPIImpl::getStreamCount() const
{
	return _streams.size();
}

const std::vector<FFmpegAPI::Stream> FFmpegAPIImpl::getStreams() const
{
	return _streams;
}

} //namespace v1
} //namespace ProtoLib
} //namespace TT
