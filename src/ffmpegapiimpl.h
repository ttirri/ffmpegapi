//
// Copyright (c) 2020 tomi.tirri@gmail.com
//
// License: GNU Lesser General Public License version 2.1 or later (LGPL v2.1+).
// SPDX-License-Identifier: LGPL-2.1+
//
#ifndef _TT_FFMPEGAPIIMPL_H
#define _TT_FFMPEGAPIIMPL_H

#include <iostream>
#include <vector>

namespace TT {
namespace ProtoLib {
inline namespace v1 {

class FFmpegAPIImpl {
	public:
		explicit FFmpegAPIImpl(std::string const &fileName);
		const std::string getFilename() const;
		const std::string getFormatString() const;
		int64_t getDurationSeconds() const;
		int getStreamCount() const;
		const std::vector<FFmpegAPI::Stream> getStreams() const;
		~FFmpegAPIImpl() = default;

		// movable
		FFmpegAPIImpl(FFmpegAPIImpl&&) noexcept;
		FFmpegAPIImpl& operator=(FFmpegAPIImpl&&) noexcept;

		// non-copyable
		FFmpegAPIImpl(const FFmpegAPIImpl& rhs) = delete;
		FFmpegAPIImpl& operator=(const FFmpegAPIImpl& rhs) = delete;

	private:
		std::string const _filename;
		std::string _format_string;
		int64_t _duration_seconds;
		std::vector<FFmpegAPI::Stream> _streams;
};

} //namespace v1
} //namespace ProtoLib
} //namespace TT
#endif

