#include "CppUTest/TestHarness.h"
#include "ffmpegapi.h"

TEST_GROUP(BasicTest)
{
};

TEST(BasicTest, TestFilename)
{
  TT::ProtoLib::FFmpegAPI lib("test");
  CHECK_EQUAL("test", lib.getFilename());
}

TEST(BasicTest, TestFormatString)
{
  TT::ProtoLib::FFmpegAPI lib("test");
  CHECK_EQUAL("FORMAT", lib.getFormatString());
}

TEST(BasicTest, TestDuration)
{
  TT::ProtoLib::FFmpegAPI lib("test");
  CHECK_EQUAL(5001, lib.getDurationSeconds());
}

TEST(BasicTest, TestStreamCount)
{
  TT::ProtoLib::FFmpegAPI lib("test");
  CHECK_EQUAL(1, lib.getStreamCount());
}

TEST(BasicTest, TestStream)
{
  TT::ProtoLib::FFmpegAPI lib("test");
  auto streams = lib.getStreams();
  CHECK_EQUAL(1, streams.size());
  for (auto stream : streams) {
    STRCMP_EQUAL("video", stream.type.c_str());
    CHECK_EQUAL(1, stream.duration);
    STRCMP_EQUAL("FOURCC", stream.fourcc.c_str());
    STRCMP_EQUAL("codec1", stream.codec.c_str());
  }
}
