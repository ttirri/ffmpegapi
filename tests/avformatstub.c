//
// Copyright (c) 2020 tomi.tirri@gmail.com
//
// License: GNU Lesser General Public License version 2.1 or later (LGPL v2.1+).
// SPDX-License-Identifier: LGPL-2.1+
//
#include "libavformat/avformat.h"

//
// Note: this is rather "raw" implementation, source code here needs polishing
// 

void av_register_all()
{
	//nothing needed
}

void avcodec_register_all()
{
	//nothing needed
}

int av_strerror(__attribute__((unused)) int errnum, __attribute__((unused)) char *errbuf, __attribute__((unused)) size_t errbuf_size)
{
	return 0;
}

char *av_fourcc_make_string(char *buf, __attribute__((unused)) uint32_t fourcc)
{
	static char buffer[] = "FOURCC";
	strcpy(buf, buffer);

	return buf;
}

int avformat_open_input(AVFormatContext **ps, __attribute__((unused)) const char *url, __attribute__((unused)) AVInputFormat *fmt, __attribute__((unused)) AVDictionary **options)
{
	static AVFormatContext format_context;
	static AVStream stream, **st;
	static struct AVInputFormat iformat;

	iformat.long_name = "FORMAT";
	format_context.iformat = &iformat;

	format_context.duration = 1;
	format_context.nb_streams = 1;

	stream.codecpar = NULL;
	stream.duration = 1;

	format_context.streams = malloc(sizeof(*st));
	format_context.streams[0] = &stream;

	*ps = &format_context;

	return 0;
}

AVCodecContext * avcodec_alloc_context3(__attribute__((unused)) const AVCodec *codec) 
{
	static AVCodecContext avcontext;

	avcontext.codec_tag = 1;
	avcontext.codec_type = AVMEDIA_TYPE_VIDEO;
	avcontext.codec_id = 1;

	return &avcontext;
}

void avformat_close_input(__attribute__((unused)) AVFormatContext **s)
{
	free((*s)->streams);
}

int avcodec_parameters_to_context(__attribute__((unused)) AVCodecContext *codec, __attribute__((unused)) const AVCodecParameters *par)
{
	return 0;
}

void avcodec_free_context(__attribute__((unused)) AVCodecContext **avctx)
{
	//nothing needed
}

const char *avcodec_get_name(__attribute__((unused)) enum AVCodecID id)
{
	return "codec1";
}

const char *av_get_media_type_string(__attribute__((unused)) enum AVMediaType media_type)
{
	return "video";
}
