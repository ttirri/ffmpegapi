#!/bin/sh

LOC=$(cd "$(dirname "$0")"; pwd) 

cmake -H$LOC/.. -B$LOC/../build
cmake --build $LOC/../build
