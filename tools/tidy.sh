#!/bin/sh

LOC=$(cd "$(dirname "$0")"; pwd) 

clang-tidy $LOC/../src/ffmpegapi.cpp -checks=* -header-filter=.* -- -std=c++11 -I $LOC/../include/
clang-tidy $LOC/../src/ffmpegapiimpl.cpp -checks=* -header-filter=.* -- -std=c++11 -I $LOC/../include/

