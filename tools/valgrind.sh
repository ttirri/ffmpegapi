#!/bin/sh

FILENAME="bbb_sunflower_2160p_60fps_normal.mp4"

LOC=$(cd "$(dirname "$0")"; pwd)

valgrind --leak-check=full --show-leak-kinds=all $LOC/../build/example/testapp $LOC/../data/$FILENAME
